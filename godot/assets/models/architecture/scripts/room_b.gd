extends Node3D

# Expose the door state to the inspector
@export var open_door: bool = false
@onready var door: AnimationPlayer = $AnimationPlayer

func _ready() -> void:
	if open_door:
		door.play("open")
