extends Node3D
@export_range(0, 1) var chance_for_tall_box: float = 0.5

func _ready() -> void:
	if randf() < chance_for_tall_box:
		$box_A.queue_free()
	else:
		$box_B.queue_free()
