class_name FacePosition extends ActionLeaf

# Called each tick of the behavior tree
func tick(actor: Node, _blackboard: Blackboard) -> int:
	# Ensure actor and its look_position are valid
	actor.look_in_move_direction()
	return SUCCESS
