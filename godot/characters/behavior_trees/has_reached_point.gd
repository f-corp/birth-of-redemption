class_name HasReachedPoint extends ConditionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.has_reached_point():
		return SUCCESS
	else:
		return FAILURE
