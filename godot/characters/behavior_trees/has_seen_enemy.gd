class_name HasSeenEnemy extends ConditionLeaf

func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.has_seen_enemy():
		return SUCCESS
	else:
		return FAILURE
