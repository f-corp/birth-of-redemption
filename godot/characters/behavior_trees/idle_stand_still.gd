class_name IdleStandStill extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.stand_still_for_seconds(randf_range(Robot1Vars.MIN_IDLE_STAND_TIME, Robot1Vars.MAX_IDLE_STAND_TIME))
	return SUCCESS

