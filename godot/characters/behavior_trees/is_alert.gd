class_name IsAlert extends ConditionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_alert():
		return SUCCESS
	else:
		return FAILURE
