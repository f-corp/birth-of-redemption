class_name IsInGoodPosition extends ConditionLeaf

func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_in_good_position():
		return SUCCESS
	else:
		return FAILURE