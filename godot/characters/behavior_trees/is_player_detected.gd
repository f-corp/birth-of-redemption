class_name IsPlayerDetected extends ConditionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_player_detected:
		return SUCCESS
	else:
		return FAILURE

