class_name IsPlayerLineOfSight extends ConditionLeaf

func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_player_line_of_sight():
		return SUCCESS
	else:
		return FAILURE
