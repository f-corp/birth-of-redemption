class_name IsStandingStill extends ConditionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_standing_still():
		return SUCCESS
	else:
		return FAILURE
