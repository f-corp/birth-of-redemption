class_name JustDoCover extends ConditionLeaf


func tick(_actor: Node, _blackboard: Blackboard) -> int:
	if Robot1Vars.just_do_cover:
		return SUCCESS
	else:
		return FAILURE

