class_name JustDoIdle extends ConditionLeaf


func tick(_actor: Node, _blackboard: Blackboard) -> int:
	if Robot1Vars.just_do_idle:
		return SUCCESS
	else:
		return FAILURE

