class_name JustDoSearch extends ConditionLeaf


func tick(_actor: Node, _blackboard: Blackboard) -> int:
	if Robot1Vars.just_do_search:
		return SUCCESS
	else:
		return FAILURE

