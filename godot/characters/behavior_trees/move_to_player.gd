class_name MoveToPlayer extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.player:
		actor.move_to_target_location(actor.player.global_transform.origin)
		return RUNNING
	else:
		return FAILURE
