class_name MoveToPosition extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.has_move_position() and not actor.has_reached_point():
		actor.move_to_target_location(actor.get_move_position())
		return SUCCESS
	else:
		return SUCCESS