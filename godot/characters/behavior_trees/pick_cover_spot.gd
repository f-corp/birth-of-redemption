class_name PickCoverSpot extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.set_move_position(actor.find_best_cover())
	actor.set_reached_point_status(false)
	return SUCCESS
