class_name PickRandomPositionIdle extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.set_move_position(actor.pick_random_position(Robot1Vars.MIN_IDLE_POSITION_RANGE, Robot1Vars.MAX_IDLE_POSITION_RANGE))
	return SUCCESS
