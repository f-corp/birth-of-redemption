class_name PickRandomPositionSearch extends ActionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.set_move_position(actor.pick_random_position(Robot1Vars.MIN_SEARCH_POSITION_RANGE, Robot1Vars.MAX_SEARCH_POSITION_RANGE))
	return SUCCESS
