class_name RunStopper extends ConditionLeaf


func tick(_actor: Node, _blackboard: Blackboard) -> int:
	""" This is only used to stop behaviors from happening when testing """
	if true == false:
		return SUCCESS
	else:
		return FAILURE

