extends ConditionLeaf


func tick(actor: Node, _blackboard: Blackboard) -> int:
	if actor.is_player_in_visual_sight():
		return SUCCESS
	else:
		return FAILURE
