class_name SetRunSpeed extends ActionLeaf

func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.set_run_speed()
	return SUCCESS
