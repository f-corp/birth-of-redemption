class_name SetWalkSpeed extends ActionLeaf

func tick(actor: Node, _blackboard: Blackboard) -> int:
	actor.set_walk_speed()
	return SUCCESS
