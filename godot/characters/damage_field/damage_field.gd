extends Node3D


@onready var area = $Area3D
@onready var timer = $Timer

var things_in_area: Array = []

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	pass


func deal_damage():
	if things_in_area:
		for thing in things_in_area:
			if thing.has_method("take_damage"):
				thing.take_damage(10)


func _on_area_3d_body_entered(body:Node3D) -> void:
	things_in_area.append(body)
	timer.start()

func _on_area_3d_body_exited(body:Node3D) -> void:
	things_in_area.erase(body)
	timer.stop()

func _on_timer_timeout() -> void:
	deal_damage()
	timer.start()
