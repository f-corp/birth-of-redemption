extends Node3D

# Create player data and prepare save/load
var save_file_path = "user://godot_bor_save/"
var save_file_name = "enemy_group_save.tres"
var EnemyGroupData = EnemyGroupDataRes.new()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var player = get_parent().get_node_or_null("player")
	EnemyGroupData.player_rid = player.get_rid()
	EnemyGroupData.cover_spots = $CoverSpots
	
	# Make the save file name unique
	var id = get_instance_id()
	save_file_name = str(id) + save_file_name

	# check save load directory
	DirAccess.make_dir_absolute(save_file_path)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	# All save and load
	if Input.is_action_just_pressed("load"):
		load_data()
	if Input.is_action_just_pressed("save"):
		save_data()

	# Set global position of player (not based on robot line of sight)
	var player = get_parent().get_node_or_null("player")
	EnemyGroupData.player_position_global = player.position

func load_data():
	EnemyGroupData = ResourceLoader.load(save_file_path + save_file_name).duplicate(true)
	on_load_data()
	print("EnemyGroup data loaded")
func save_data():
	on_save_data()
	ResourceSaver.save(EnemyGroupData, save_file_path + save_file_name)
	print("EnemyGroup data saved")
func on_load_data():
	# the cover spots needs to be set on load
	EnemyGroupData.cover_spots = $CoverSpots
func on_save_data():
	pass
