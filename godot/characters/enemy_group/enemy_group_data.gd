extends Resource
class_name EnemyGroupDataRes

var cover_spots : GridMap

@export var taken_cover_spots : Dictionary

@export var is_alert: bool = false
@export var has_seen_enemy: bool = false
@export var is_player_detected: bool = false

@export var player_position : Vector3 = Vector3.ZERO
@export var player_position_global: Vector3 = Vector3.ZERO

@export var player_rid = null

