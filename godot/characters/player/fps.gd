extends Label

var x_indent = 1
var y_indent = 1

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	place_fps_counter()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_fullscreen"):
		place_fps_counter()


func place_fps_counter():
	var window_size =  DisplayServer.window_get_size()
	
	position.x = int(0.01 * x_indent * window_size.x)
	position.y = int(0.01 * y_indent * window_size.y)
