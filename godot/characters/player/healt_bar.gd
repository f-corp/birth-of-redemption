extends ProgressBar

# Indent and size of health bar in percent of window size
var x_indent: float = 5
var y_indent: float = 5

var x_size: float = 15
var y_size: float = 5

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	place_health_bar()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("ui_fullscreen"):
		place_health_bar()


func place_health_bar():
	var window_size =  DisplayServer.window_get_size()
	
	size.x = int(0.01 * x_size * window_size.x)
	position.x = int(0.01 * x_indent * window_size.x)

	size.y = int(0.01 * y_size * window_size.y)
	position.y = int(0.01 * (100-y_indent) * window_size.y - size.y)

func update_health_bar(health_value):
	value = health_value
