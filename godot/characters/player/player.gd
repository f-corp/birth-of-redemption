extends CharacterBody3D

# Create player data and prepare save/load
var save_file_path = "user://godot_bor_save/"
var save_file_name = "player_save.tres"
var PlayerData = PlayerDataRes.new()

@onready var head = $Head
@onready var collision_shape = $CollisionShape3D
@onready var mesh = $MeshInstance3D
@onready var top_cast = $TopCast
@onready var camera3D = $Head/Camera3D
@onready var step_detector = $StepDetector
@onready var ray_to_floor = $RayToFloor
@onready var forward_detector = $Head/ForwardDetector
@onready var fps_counter = $Head/Camera3D/HUD/FPS
@onready var health_bar = $Head/Camera3D/HUD/HealtBar


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	camera3D.fov = PlayerData.CAMERA_FOV
	collision_shape.shape.height = PlayerData.STAND_HEIGHT

	# check save load directory
	DirAccess.make_dir_absolute(save_file_path)

	# HUD
	update_health_bar()


func _physics_process(delta: float) -> void:
	if not is_on_floor():
		velocity += get_gravity() * delta
	else:
		PlayerData.is_jumping = false

	# All player inputs
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = PlayerData.JUMP_VELOCITY
		PlayerData.is_jumping = true
	elif Input.is_action_pressed("crouch") or top_cast.is_colliding():
		crouch(delta, true)
		PlayerData.move_speed = PlayerData.CROUCH_SPEED
	else:
		crouch(delta, false)
	
	if Input.is_action_pressed("sprint") and is_on_floor() and not PlayerData.is_crouching:
		PlayerData.move_speed = PlayerData.SPRINT_SPEED
	else:
		if not PlayerData.is_crouching:
			PlayerData.move_speed = PlayerData.SPEED 
	
	# Set robot move position to left mouse click position straigt forward (just for testing)
	if Input.is_action_just_pressed("attack"):
		if forward_detector.get_collider() and forward_detector.get_collider().is_in_group("Enemies"):
			forward_detector.get_collider().take_damage(5)

	# Get the input direction and handle the movement/deceleration.
	var input_dir := Input.get_vector("move_left", "move_right", "move_forward", "move_backward")
	var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var acc = PlayerData.ACCELERATION if is_on_floor() else PlayerData.AIR_ACCELERATION
	
	# Check if player base is colliding with a step and help get over it
	if step_detector.is_colliding() and (not is_on_floor() or step_detector.get_collision_normal(0).dot(Vector3.UP) > 0.99): # do not climb ramps
		# Get the collision point and calculate the direction to it
		var collision_point = step_detector.get_collision_point(0)
		var to_collision = (collision_point - global_transform.origin).normalized()

		# Check if the player is moving towards the step
		if direction.dot(to_collision) > 0:  # Positive dot product means facing the step
			# Smoothly adjust vertical velocity towards the step height
			var step_height_diff = collision_point.y - global_transform.origin.y
			if step_height_diff > 0 and step_height_diff <= PlayerData.MAX_STEP_HEIGHT:
				velocity.y = lerp(velocity.y, step_height_diff / delta, 0.05)  # Smoothly move upward

	if direction:
		velocity.x = lerp(velocity.x, direction.x * PlayerData.move_speed, acc*delta)
		velocity.z = lerp(velocity.z, direction.z * PlayerData.move_speed, acc*delta)
	else:
		velocity.x = lerp(velocity.x, 0.0, acc*delta)
		velocity.z = lerp(velocity.z, 0.0, acc*delta)

	move_and_slide()

	head.rotation_degrees.x = PlayerData.look_rotation.x
	rotation_degrees.y = PlayerData.look_rotation.y


func _process(_delta: float) -> void:
	# Fps counter
	if Input.is_action_just_pressed("fps_toggle"):
		fps_counter.visible = !fps_counter.visible
	if fps_counter.visible:
		fps_counter.text = "FPS " + str(Engine.get_frames_per_second())
	# Invisibility toggle
	if Input.is_action_just_pressed("invisible"):
		PlayerData.is_invisible = !PlayerData.is_invisible
		print("invisible: ", PlayerData.is_invisible)
	
	if Input.is_action_just_pressed("load"):
		load_data()
	if Input.is_action_just_pressed("save"):
		save_data()
	

func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		PlayerData.look_rotation.y -= (event.relative.x*PlayerData.MOUSE_SENSITIVITY)
		PlayerData.look_rotation.x -= (event.relative.y*PlayerData.MOUSE_SENSITIVITY)
		PlayerData.look_rotation.x = clamp(PlayerData.look_rotation.x, PlayerData.MINIMUM_LOOK_ANG, PlayerData.MAXIMUM_LOOK_ANG)

# All save and load
func load_data():
	PlayerData = ResourceLoader.load(save_file_path + save_file_name).duplicate(true)
	on_load_data()
	print("Player data loaded")
func save_data():
	on_save_data()
	ResourceSaver.save(PlayerData, save_file_path + save_file_name)
	print("Player data saved")
func on_load_data():
	self.position = PlayerData.save_position
func on_save_data():
	PlayerData.update_save_position(global_position)


func update_health_bar():
	health_bar.update_health_bar(PlayerData.health)

func take_damage(damage : float):
	PlayerData.health -= damage
	update_health_bar()

	if PlayerData.health <= 0:
		die()

func die():
	print("You Died!")


func crouch(delta : float, crouching = true):
	PlayerData.is_crouching = true if crouching else false
	
	var target_height : float = PlayerData.CROUCH_HEIGHT if crouching else PlayerData.STAND_HEIGHT

	collision_shape.shape.height = lerp(collision_shape.shape.height, target_height, delta*PlayerData.CROUCH_TRANSITION_TIME)
	mesh.mesh.height = lerp(mesh.mesh.height, target_height, delta*PlayerData.CROUCH_TRANSITION_TIME)

	if PlayerData.is_crouching and PlayerData.is_jumping:
		collision_shape.position.y = lerp(collision_shape.position.y, target_height * 1.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
		mesh.position.y = lerp(mesh.position.y, target_height * 1.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
		head.position.y = lerp(head.position.y, PlayerData.STAND_HEIGHT - 0.13, delta*PlayerData.CROUCH_TRANSITION_TIME)
		# Set step_detector height to match player height
		step_detector.position.y = lerp(step_detector.position.y, 1.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
	else:
		collision_shape.position.y = lerp(collision_shape.position.y, target_height * 0.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
		mesh.position.y = lerp(mesh.position.y, target_height * 0.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
		head.position.y = lerp(head.position.y, target_height - 0.13, delta*PlayerData.CROUCH_TRANSITION_TIME)
		# Set step_detector height to match player height
		step_detector.position.y = lerp(step_detector.position.y, 0.5, delta*PlayerData.CROUCH_TRANSITION_TIME)
