extends Resource
class_name PlayerDataRes

"""
All player data is stored as Resource
This information is controls the players hole current status and can be saved and loaded
"""

# Constants
const SPEED := PlayerVars.SPEED
const ACCELERATION := PlayerVars.ACCELERATION
const AIR_ACCELERATION := PlayerVars.AIR_ACCELERATION
const JUMP_VELOCITY := PlayerVars.JUMP_VELOCITY

const SPRINT_SPEED := PlayerVars.SPRINT_SPEED
const STAND_HEIGHT := PlayerVars.STAND_HEIGHT

const CROUCH_HEIGHT := PlayerVars.CROUCH_HEIGHT
const CROUCH_TRANSITION_TIME := PlayerVars.CROUCH_TRANSITION_TIME
const CROUCH_SPEED := PlayerVars.CROUCH_SPEED

const MAX_STEP_HEIGHT := PlayerVars.MAX_STEP_HEIGHT

const MINIMUM_LOOK_ANG := PlayerVars.MINIMUM_LOOK_ANG
const MAXIMUM_LOOK_ANG := PlayerVars.MAXIMUM_LOOK_ANG

const MAX_HEALTH := PlayerVars.MAX_HEALTH
const HEALTH_REGEN := PlayerVars.HEALTH_REGEN

# Menue edits
const MOUSE_SENSITIVITY := PlayerVars.MOUSE_SENSITIVITY_BASE*0.01
const CAMERA_FOV := PlayerVars.CAMERA_FOV_BASE

# Variables
@export var look_rotation : Vector2
@export var move_speed : float
@export var save_position : Vector3

@export var health := MAX_HEALTH

# state variables
@export var is_jumping : bool = false
@export var is_on_step : bool = false
@export var is_crouching: bool = false

# Debug variables
@export var is_invisible: bool = false


func update_save_position(value: Vector3):
    save_position = value

