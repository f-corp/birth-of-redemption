extends Node

# Movement
const SPEED: float = 5.0
const ACCELERATION: float = 6
const AIR_ACCELERATION: float = 1
const JUMP_VELOCITY: float = 4.5

const SPRINT_SPEED: float = 8
const STAND_HEIGHT: float = 1.93

const CROUCH_HEIGHT: float = 0.965
const CROUCH_TRANSITION_TIME: float = 8
const CROUCH_SPEED: float = 3

const MAX_STEP_HEIGHT: float = 0.2

# View
const MINIMUM_LOOK_ANG: float = -80
const MAXIMUM_LOOK_ANG: float = 90

# Health and combat
const MAX_HEALTH: float = 100
const HEALTH_REGEN: float = 5


# standard values
const MOUSE_SENSITIVITY_BASE: float = 4
const CAMERA_FOV_BASE: float = 75
