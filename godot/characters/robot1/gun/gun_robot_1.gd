extends Node3D

var RANDOM_SPREAD = Robot1Vars.SHOT_RANDOM_SPREAD


func _ready() -> void:
	pass


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("attack"):
		shoot()


func shoot():
	var nozzle = $Nozzle
	# Get the direction the nozzle is pointing (forward direction in local space is -Z in 3D)
	var aim_dir = nozzle.global_transform.basis.y.normalized()

	# Load the scene for the projectile
	var scene = preload("res://characters/robot1/gun/shot/shot.tscn")
	var shot = scene.instantiate()  # Use instantiate() in Godot 4
	
	# Pass the direction to the projectile
	var random_spread = Vector3(rand_normal(0, RANDOM_SPREAD), rand_normal(0, RANDOM_SPREAD), rand_normal(0, RANDOM_SPREAD))
	shot.direction = aim_dir + random_spread
	
	# Add the shot as a child of the nozzle so it can move independently
	nozzle.add_child(shot)

func rand_normal(mean: float = 0.0, std_dev: float = 1.0) -> float:
	"Fancy normal distributed random function, thank you deepseek!"
	var rng = RandomNumberGenerator.new()
	rng.randomize()
    
    # Generate two uniformly distributed random numbers
	var u1 = rng.randf()
	var u2 = rng.randf()
    
    # Apply the Box-Muller transform
	var z0 = sqrt(-2.0 * log(u1)) * cos(2.0 * PI * u2)
    
    # Scale and shift to match the desired mean and standard deviation
	return z0 * std_dev + mean
