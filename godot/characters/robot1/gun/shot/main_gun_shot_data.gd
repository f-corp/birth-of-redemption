extends Resource
class_name MainGunShotData

var GRAVITY_MULTIPLYER := Robot1Vars.WEAPON_GRAVITY_MULTIPLYER
var SPEED := Robot1Vars.WEAPON_SPEED
var REMOVE_TIMER_AFTER_HIT := Robot1Vars.SHOT_REMOVE_TIMER_AFTER_HIT

@export var save_position: Vector3
@export var save_rotation: Vector3

@export var is_traveling: bool = true


# Save/Load help
func update_save_position(value: Vector3):
    save_position = value
func update_save_rotation(value: Vector3):
    save_rotation = value
