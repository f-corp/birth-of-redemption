extends RigidBody3D

# Create player data and prepare save/load
var save_file_path = "user://godot_bor_save/"
var save_file_name = "robot1_save.tres"
var ShotData = MainGunShotData.new()

 
var velocity: Vector3
var direction: Vector3 = Vector3.ONE


@onready var remove_timer = $RemoveTimer


func _ready() -> void:
	top_level = true # Make the shot not move with parent robot
	velocity = direction*ShotData.SPEED

	# Remove timer
	remove_timer.wait_time = ShotData.REMOVE_TIMER_AFTER_HIT

	# Make the save file name unique
	var id = get_instance_id()
	save_file_name = str(id) + save_file_name

	# check save load directory
	DirAccess.make_dir_absolute(save_file_path)


func _physics_process(delta: float) -> void:
	# Shots are affected by gravity
	velocity.y -= 9.82*delta * ShotData.GRAVITY_MULTIPLYER
	move_and_collide(velocity)


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("load"):
		# remove unsaved shots before calling load
		if not ResourceLoader.exists(save_file_path + save_file_name):
			queue_free()
		else:
			load_data()
	if Input.is_action_just_pressed("save"):
		save_data()

# All save and load
func load_data():
	ShotData = ResourceLoader.load(save_file_path + save_file_name).duplicate(true)
	on_load_data()
	on_load_data()
func save_data():
	on_save_data()
	ResourceSaver.save(ShotData, save_file_path + save_file_name)

func on_load_data():
	if not ShotData.is_traveling:
		return
	unhit()
	self.position = ShotData.save_position
	self.rotation = ShotData.save_rotation

func on_save_data():
	ShotData.update_save_position(global_position)
	ShotData.update_save_rotation(rotation)


func hit():
	# disable the shot
	ShotData.is_traveling = false
	self.visible = false		# might be true for debug reason
	set_physics_process(false) 	# normal process needs to be on to enable save/load
	$Area3D.set_deferred("monitoring", false)
	
	# everything else
	print("Explode!")
	remove_timer.start()

func unhit():
	"Used for save/load"
	ShotData.is_traveling = true
	self.visible = true		# might be true for debug reason
	set_physics_process(true) 	# normal process needs to be on to enable save/load
	$Area3D.set_deferred("monitoring", true)

func _on_area_3d_body_entered(_body:Node3D) -> void:
	hit()

func _on_remove_timer_timeout() -> void:
	# Remove the shot after a coulple of seconds if it's not saved
	if not ResourceLoader.exists(save_file_path + save_file_name):
		queue_free()
