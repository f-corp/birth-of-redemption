extends Resource
class_name Robot1DataRes

# Movement
const SPEED_RUN := Robot1Vars.SPEED_RUN
const SPEED_WALK := Robot1Vars.SPEED_WALK
const ACCELERATION := Robot1Vars.ACCELERATION

# Sight
const FOV := Robot1Vars.FOV
const SIGHT_RANGE := Robot1Vars.SIGHT_RANGE
const SIGHT_HEIGHT := Robot1Vars.SIGHT_HEIGHT

# Health
const MAX_HEALTH := Robot1Vars.MAX_HEALTH

# Behavior
const OPTIMAL_DISTANCE_TO_PLAYER_MIN := Robot1Vars.OPTIMAL_DISTANCE_TO_PLAYER_MAX
const OPTIMAL_DISTANCE_TO_PLAYER_MAX := Robot1Vars.OPTIMAL_DISTANCE_TO_PLAYER_MAX
const OPTIMAL_DISTANCE_TO_PLAYER_WEIGHT := Robot1Vars.OPTIMAL_DISTANCE_TO_PLAYER_WEIGHT
const SPOT_NOT_IN_COVER_PUNISHMENT := Robot1Vars.SPOT_NOT_IN_COVER_PUNISHMENT

const SPREAD_OUT_WEIGHT := Robot1Vars.SPREAD_OUT_WEIGHT
const SPREAD_OUT_RANGE := Robot1Vars.SPREAD_OUT_RANGE
const SELF_DIST_TO_COVER_WEIGHT := Robot1Vars.SELF_DIST_TO_COVER_WEIGHT

const MIN_IDLE_STAND_TIME := Robot1Vars.MIN_IDLE_STAND_TIME
const MAX_IDLE_STAND_TIME := Robot1Vars.MAX_IDLE_STAND_TIME
const MAX_IDLE_POSITION_RANGE := Robot1Vars.MAX_IDLE_POSITION_RANGE
const MIN_IDLE_POSITION_RANGE := Robot1Vars.MIN_IDLE_POSITION_RANGE

const MIN_SEARCH_POSITION_RANGE := Robot1Vars.MIN_SEARCH_POSITION_RANGE
const MAX_SEARCH_POSITION_RANGE := Robot1Vars.MAX_SEARCH_POSITION_RANGE

const ACCEPTED_DISTANCE_TO_COVER := Robot1Vars.ACCEPTED_DISTANCE_TO_COVER


# Weapon variables
const WEAPON_SPEED := Robot1Vars.WEAPON_SPEED
const WEAPON_GRAVITU_MULTIPLYER := Robot1Vars.WEAPON_GRAVITY_MULTIPLYER


# Health
@export var health: float = MAX_HEALTH
@export var is_alive: bool = true

# Behaviors
@export var just_do_idle: bool = false
@export var just_do_search: bool = false
@export var just_do_cover: bool = false

# States
@export var has_reached_point: bool = true
@export var is_standing_still: bool = false

# Movement
@export var move_position: Vector3 = Vector3.ZERO
@export var speed = Robot1Vars.SPEED_RUN

@export var save_position: Vector3
@export var save_rotation: Vector3


# Save/Load help
func update_save_position(value: Vector3):
	save_position = value
func update_save_rotation(value: Vector3):
	save_rotation = value
