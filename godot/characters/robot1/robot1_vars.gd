extends Node

# Movement
const SPEED_RUN: float = 3.0
const SPEED_WALK: float = 1.0
const ACCELERATION: float = 0.2

# Sight
const FOV: float = 130.0
const SIGHT_RANGE: float = 20
const SIGHT_HEIGHT: float = 10

# Health
const MAX_HEALTH: float = 100


# Behavior
const OPTIMAL_DISTANCE_TO_PLAYER_MIN: float = 10**2    # the square of the distance is used in calculation
const OPTIMAL_DISTANCE_TO_PLAYER_MAX: float = 18**2
const OPTIMAL_DISTANCE_TO_PLAYER_WEIGHT: float = 1.5
const SPOT_NOT_IN_COVER_PUNISHMENT: float = 1000

const SPREAD_OUT_WEIGHT: float = 2.0
const SPREAD_OUT_RANGE: float = 300
const SELF_DIST_TO_COVER_WEIGHT: float = 0.3

const MIN_IDLE_STAND_TIME: float = 1
const MAX_IDLE_STAND_TIME: float = 3
const MAX_IDLE_POSITION_RANGE: float = 1 
const MIN_IDLE_POSITION_RANGE: float = 7

const MIN_SEARCH_POSITION_RANGE: float = 2
const MAX_SEARCH_POSITION_RANGE: float = 3

const ACCEPTED_DISTANCE_TO_COVER: float = 1.5

# Weapon
const WEAPON_SPEED: float = 0.1
const WEAPON_GRAVITY_MULTIPLYER: float = 0.0001
const SHOT_REMOVE_TIMER_AFTER_HIT: float = 6
const SHOT_RANDOM_SPREAD: float = 0.1

