extends CharacterBody3D

# Create player data and prepare save/load
var save_file_path = "user://godot_bor_save/"
var save_file_name = "robot1_save.tres"
var Robot1Data = Robot1DataRes.new()


@onready var idle_stand_still_timer = $IdleStandStillTimer

@onready var robot_head = $Head
@onready var gun = $Gun
@onready var visual_area = $VisionArea
@onready var line_of_sight_check = $LineOfSightCheck

@onready var direction_test = $DirectionCoverTest
	
@onready var health_bar = $SubViewport/HealthBar
@onready var health_bar_sprite = $HealthBarSprite

@onready var nav_agent = $NavigationAgent3D
@onready var beehave_tree = $Robot1_beehave
@onready var group = get_parent()


var player = null

func _ready() -> void:
	# Make robot avoid each other
	nav_agent.avoidance_enabled = true
	nav_agent.radius = 3
	
	# Set sight range
	visual_area.get_node("CollisionShape3D").shape.height = Robot1Data.SIGHT_HEIGHT
	visual_area.get_node("CollisionShape3D").shape.radius = Robot1Data.SIGHT_RANGE
	line_of_sight_check.target_position = Vector3(0, 0, -Robot1Data.SIGHT_RANGE)

	# Health bar
	health_bar.max_value = Robot1Data.MAX_HEALTH
	health_bar.value = Robot1Data.health


	# Make the save file name unique
	var id = get_instance_id()
	save_file_name = str(id) + save_file_name

	# check save load directory
	DirAccess.make_dir_absolute(save_file_path)


func _physics_process(delta: float) -> void:
	if is_player_in_visual_sight():
		group.EnemyGroupData.player_position = player.position
	
	is_player_line_of_sight()
	if group.EnemyGroupData.player_position:
		rotate_gun_up_down(group.EnemyGroupData.player_position, 10, delta)


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("just_do_behavior1"):
		Robot1Data.just_do_idle = true
		Robot1Data.just_do_search = false
		Robot1Data.just_do_cover = false
	elif Input.is_action_just_pressed("just_do_behavior2"):
		Robot1Data.just_do_idle = false
		Robot1Data.just_do_search = true
		Robot1Data.just_do_cover = false
	elif Input.is_action_just_pressed("just_do_behavior3"):
		Robot1Data.just_do_idle = false
		Robot1Data.just_do_search = false
		Robot1Data.just_do_cover = true
	
	# Health bar
	if Input.is_action_just_pressed("health_bar_toggle"):
		health_bar_sprite.visible = !health_bar_sprite.visible

	if Input.is_action_just_pressed("load"):
		load_data()
	if Input.is_action_just_pressed("save"):
		save_data()

# All save and load
func load_data():
	Robot1Data = ResourceLoader.load(save_file_path + save_file_name).duplicate(true)
	on_load_data()
	print("Robot1 data loaded")
func save_data():
	on_save_data()
	ResourceSaver.save(Robot1Data, save_file_path + save_file_name)
	print("Robot1 data saved")

func on_load_data():
	if not Robot1Data.is_alive:
		return
	undie() # make sure that all vital signs is on
	self.position = Robot1Data.save_position
	self.rotation = Robot1Data.save_rotation
	health_bar.value = Robot1Data.health

func on_save_data():
	Robot1Data.update_save_position(global_position)
	Robot1Data.update_save_rotation(rotation)


func idle_stressed():
	rotation.y = lerp(rotation.y, rotation.y + 1, 0.2)

func move_to_target_location(target_location):
	nav_agent.set_target_position(target_location)
	var current_location = global_transform.origin
	var next_location = nav_agent.get_next_path_position()
	var new_velocity = (next_location-current_location).normalized() * Robot1Data.speed
	
	velocity = lerp(velocity, new_velocity, Robot1Data.ACCELERATION)
	move_and_slide()

func look_in_move_direction():
	var target_direction = velocity.normalized()
	# Calculate the target rotation (Y-axis only)
	var target_rotation = Vector3(0, atan2(target_direction.x, target_direction.z), 0)
	# Apply the smoothed rotation
	rotation.y = lerp_angle(rotation.y, target_rotation.y, 0.08)

func look_at_player_position():
	var target_direction = (group.EnemyGroupData.player_position - position).normalized()
	var target_rotation = Vector3(0, atan2(target_direction.x, target_direction.z), 0)
	# # Apply the smoothed rotation
	rotation.y = lerp_angle(rotation.y, target_rotation.y, 0.08)

func rotate_gun_up_down(target_position: Vector3, speed: float, delta: float):
	var height_offset = 0.6			# Account for player and gun height
	var angle_offset_rad = PI/2		# The gun might be rotated from start

	# Calculate angle from distance in x & y
	var gun_pos = gun.global_transform.origin
	var y_diff = gun_pos.y - (target_position.y + height_offset)
	var x_diff = gun_pos.distance_to(target_position)
	var target_angle = atan2(x_diff, y_diff) - angle_offset_rad

	gun.rotation.z = lerp_angle(gun.rotation.z, target_angle, speed*delta)



func pick_random_position(min_distance: float = 0.0, max_distance: float = 10.0) -> Vector3:
	"Pick a random spot"
	var random_angle = randf_range(0, 2 * PI)
	var random_distance = randf_range(min_distance, max_distance)
	var offset_x = cos(random_angle) * random_distance
	var offset_z = sin(random_angle) * random_distance

	var random_point = global_transform.origin + Vector3(offset_x, 0, offset_z)
	Robot1Data.has_reached_point = false
	return random_point

func stand_still_for_seconds(time:float = 5):
	"Set move posision to current position and set is_standing still until timer (signal)"
	Robot1Data.move_position = position
	if not Robot1Data.is_standing_still:
		idle_stand_still_timer.start(time)
		Robot1Data.is_standing_still = true

func start_move_from_idle():
	Robot1Data.is_standing_still = false


func take_damage(value: float):
	Robot1Data.health -= value

	if Robot1Data.health >= 0:
		health_bar.value = Robot1Data.health
		group.EnemyGroupData.is_alert = true
	else:
		die()

func die():
	"Disable all robot functions upon death"
	Robot1Data.is_alive = false
	self.visible = false		# might be true for debug reason
	set_physics_process(false) 	# normal process needs to be on to enable save/load
	$CollisionShape3D.disabled = true
	beehave_tree.enabled = false

func undie():
	"Reset dead robot, mostly for save/load"
	Robot1Data.is_alive = true
	self.visible = true
	set_physics_process(true)
	$CollisionShape3D.disabled = false
	beehave_tree.enabled = true



func find_best_cover():
	"Calculate the cover spot in the cover pots gridmap that has max distance to player"
	var cover_spots_full = group.EnemyGroupData.cover_spots.get_used_cells_by_item(0) # get a list of all available cover spots
	# DirectionTest is a raycast and should not collide with robot
	direction_test.add_exception_rid(get_rid())

	var best_spot: Vector3 = cover_spots_full[0]
	var best_spot_points = 10000
	# give the dictionary of cover spots a value
	group.EnemyGroupData.taken_cover_spots[get_rid()] = best_spot

	for cover_spot: Vector3 in cover_spots_full:
		# All spots are given a score. The lowest is the best
		var spot_points = 0

		# Check if there is a wall between cover spot and player
		# direction_test.global_position = cover_spot
		# direction_test.global_position += Vector3(0.5, 0.3, 0.5) # center the raycast on cover spot
		# direction_test.look_at(group.EnemyGroupData.player_position, Vector3.UP)
		# direction_test.force_raycast_update()

		# Punish spot if it don't provide cover
		# var not_in_cover_punishment = 0
		# if not direction_test.is_colliding() or direction_test.get_collider() == player:
		# 	not_in_cover_punishment = Robot1Data.SPOT_NOT_IN_COVER_PUNISHMENT
		# spot_points += not_in_cover_punishment
		
		# Check if the cover spot is in good range of player
		var square_dist_optimal_min = Robot1Data.OPTIMAL_DISTANCE_TO_PLAYER_MIN
		var square_dist_optimal_max = Robot1Data.OPTIMAL_DISTANCE_TO_PLAYER_MAX
		var square_dist_cover_spot_player = cover_spot.distance_squared_to(group.EnemyGroupData.player_position)
		var dist_to_optimal_punishment = 0

		if square_dist_cover_spot_player < square_dist_optimal_min:
			dist_to_optimal_punishment = square_dist_optimal_min - square_dist_cover_spot_player
		elif square_dist_cover_spot_player > square_dist_optimal_max:
			dist_to_optimal_punishment = square_dist_cover_spot_player - square_dist_optimal_max
		dist_to_optimal_punishment *= Robot1Data.OPTIMAL_DISTANCE_TO_PLAYER_WEIGHT
		
		# Robot1Vars.OPTIMAL_DISTANCE_TO_PLAYER_WEIGHT
		spot_points += dist_to_optimal_punishment

		# Check if the cover spot is taken
		var total_taken_spot_punishment = 0
		for taken_spot in group.EnemyGroupData.taken_cover_spots.values():
			if not group.EnemyGroupData.taken_cover_spots[get_rid()] == taken_spot:
				var taken_spot_points = Robot1Data.SPREAD_OUT_RANGE - cover_spot.distance_squared_to(taken_spot)
				if taken_spot_points < 0:
					taken_spot_points = 0
				elif taken_spot_points == 200:
					taken_spot_points = 1000
				total_taken_spot_punishment += taken_spot_points*Robot1Data.SPREAD_OUT_WEIGHT
		
		# Divide by total number of enemies
		var total_friends = (len(group.EnemyGroupData.taken_cover_spots.values()) - 1)
		if total_friends <= 0:
			total_friends = 1
		total_taken_spot_punishment /= total_friends
		spot_points += total_taken_spot_punishment

		# Check distance from self to spot
		var square_dist_self_spot_punishment = cover_spot.distance_squared_to(position)
		square_dist_self_spot_punishment *= Robot1Data.SELF_DIST_TO_COVER_WEIGHT
		spot_points += square_dist_self_spot_punishment

		# Print to enable comparison of all spot punishments
		# print([
		# 	not_in_cover_punishment,
		# 	dist_to_optimal_punishment,
		# 	total_taken_spot_punishment,
		# 	square_dist_self_spot_punishment,
		# ])

		# Evaluate if spot is better than previous spots
		if spot_points < best_spot_points:
			best_spot_points = spot_points
			best_spot = cover_spot


	# Set the best cover spot as taken and return it
	group.EnemyGroupData.taken_cover_spots[get_rid()] = best_spot
	return best_spot

func point_raycast_to_position(raycast: RayCast3D, target_position: Vector3):
	var origin_position = raycast.global_transform.origin
	# Calculate the direction to the targets eyes
	var direction = (origin_position - (target_position + Vector3(0,1.8,0))).normalized()
	# Create a new basis using Basis.looking_at() statically
	var new_basis = Basis.looking_at(direction, Vector3.UP)
	# Update the global transform of the RayCast3D
	raycast.global_transform = Transform3D(new_basis, origin_position)

func set_run_speed():
	Robot1Data.speed = Robot1Data.SPEED_RUN

func set_walk_speed():
	Robot1Data.speed = Robot1Data.SPEED_WALK


func _on_vision_area_body_entered(body: Node3D) -> void:
	if body.name == "player":
		player = body

# State checks
func is_alert():
	if group.EnemyGroupData.is_alert:
		return true
	else:
		return false

func has_move_position():
	if not Robot1Data.move_position == Vector3.ZERO:
		return true
	else:
		return false

func is_standing_still():
	if Robot1Data.is_standing_still:
		return true
	else:
		return false

func has_reached_point():
	if Robot1Data.has_reached_point:
		return true
	else:
		return false

func has_seen_enemy():
	if group.EnemyGroupData.has_seen_enemy:
		return true
	else:
		return false

func is_in_good_position():
	"Robot checks if it is close enough to the ideal cover position
	This also marks the cover position as 'taken' for other robots"
	var best_cover_position: Vector3 = find_best_cover()
	
	var dist_to_cover = global_transform.origin.distance_squared_to(best_cover_position)

	if dist_to_cover <= Robot1Data.ACCEPTED_DISTANCE_TO_COVER:
		return true
	else:
		return false

func is_player_line_of_sight():
	""" Vision check to see if player is line of sight of robot """
	var heights = [0.1, 0.5, 0.9, 1,4, 1,8]
	for height in heights:
		line_of_sight_check.look_at(group.EnemyGroupData.player_position_global + Vector3(0, height, 0), Vector3.UP)
		line_of_sight_check.force_raycast_update()
		if line_of_sight_check.get_collider_rid() == group.EnemyGroupData.player_rid:
			if line_of_sight_check.get_collider().PlayerData.is_invisible:
				return false
			return true
	return false

func is_player_in_visual_sight():
	if player:
		if player.PlayerData.is_invisible:
			return false
		var direction = global_position.direction_to(player.global_position)
		var facing = global_transform.basis.tdotz(direction)
		
		var fov = cos(deg_to_rad(Robot1Data.FOV/2))

		if facing > fov:
			if is_player_line_of_sight():
				group.EnemyGroupData.is_player_detected = true
				group.EnemyGroupData.has_seen_enemy = true
				group.EnemyGroupData.is_alert = true
				return true
		else:
			group.EnemyGroupData.is_player_detected = false
			return false
	else:
		group.EnemyGroupData.is_player_detected = false
		return false


func set_reached_point_status(value: bool):
	Robot1Data.has_reached_point = value

func get_move_position():
	return Robot1Data.move_position
func set_move_position(value: Vector3):
	Robot1Data.move_position = value

func _on_vision_area_body_exited(body: Node3D) -> void:
	if body.name == "player":
		player = null


func _on_navigation_agent_3d_navigation_finished() -> void:
	Robot1Data.has_reached_point = true


func _on_navigation_agent_3d_target_reached() -> void:
	pass


func _on_idle_stand_still_timer_timeout() -> void:
	start_move_from_idle()
