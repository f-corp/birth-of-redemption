extends Node3D

func _ready() -> void:
	pass

# Called whenever an input event is detected
func _input(_event):
	if Input.is_action_just_pressed("ui_fullscreen"):
		if DisplayServer.window_get_mode() != DisplayServer.WINDOW_MODE_WINDOWED:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_MAXIMIZED)
	elif Input.is_action_just_pressed("exit"):
		get_tree().quit()
