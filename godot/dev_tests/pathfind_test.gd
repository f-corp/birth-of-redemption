extends Node3D


@onready var player = $player

# Called when the node is added to the scene.
func _ready():
	# Ensure the game starts in full screen
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)

# Called whenever an input event is detected
func _input(_event):
	# Check if the input action 'ui_fullscreen' is pressed
	if Input.is_action_just_pressed("ui_fullscreen"):
		toggle_fullscreen()
	if Input.is_action_just_pressed("exit"):
		get_tree().quit()


func _physics_process(_delta: float) -> void:
	get_tree().call_group("Robot1s", "update_target_location", player.global_transform.origin)


# Function to toggle full screen mode
func toggle_fullscreen():
	if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_FULLSCREEN:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)

# Function to toggle mouse visibility
func toggle_mouse_visibility():
	if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		Input.mouse_mode = Input.MOUSE_MODE_HIDDEN
	else:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
