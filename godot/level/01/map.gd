extends Node3D
@export var level: String
@export var bake_lights: bool = true

func _ready() -> void:
	if bake_lights:
		var start_time = Time.get_ticks_msec()
		print(level, " baking")
		$VoxelGI.bake()
		print(level, " baked in: ", Time.get_ticks_msec() - start_time, " ms")


func _input(_event):
	if Input.is_action_just_pressed("ui_fullscreen"):
		if DisplayServer.window_get_mode() != DisplayServer.WINDOW_MODE_WINDOWED:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_MAXIMIZED)
	elif Input.is_action_just_pressed("exit"):
		get_tree().quit()